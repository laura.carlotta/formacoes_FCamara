Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Origem: Wikipédia, a enciclopédia livre.
Ver também: Mozilla Foundation e Mozilla Corporation
Translation Latin Alphabet.svg
	
Este artigo ou se(c)ção está a ser traduzido de «Mozilla» na Wikipédia em inglês Ajude e colabore com a tradução. (Abril de 2017)
Mozilla
Atividade 	Software de código aberto
Fundação 	28 de fevereiro de 1998
Fundador(es) 	Netscape Communications Corporation
Divisões 	

Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Origem: Wikipédia, a enciclopédia livre.
Ver também: Mozilla Foundation e Mozilla Corporation
    Mozilla Corporation
    Mozilla Foundation

Website oficial 	mozilla.org

A Mozilla (estilizado como moz://a) é uma comunidade de software livre criada em 1998 por membros da Netscape. A comunidade Mozilla usa, desenvolve, divulga e suporta os produtos Mozilla, assim promovendo exclusivamente software livre e padrões abertos, com algumas menores exceções.[nota 1] A comunidade é suportada institucionalmente pela Mozilla Foundation e a sua subsidiária contribuinte, a Mozilla Corporation.[1]

A Mozilla produziu muitos produtos como o navegador web Firefox, o cliente de e-mail Thunderbird, o sistema operacional móvel Firefox OS, o sistema de rastreamento de bugs Bugzilla, o motor de layout Gecko e outros projetos.
Índice

    1 História
    2 Valores
        2.1 Compromisso
    3 Software
        3.1 Firefox
        3.2 Firefox Mobile
        3.3 Firefox OS
        3.4 Thunderbird
        3.5 SeaMonkey
    4 Ver também
    5 Notas
    6 Referências
    7 Ligações externas

História

Em 23 de janeiro de 1998, a Netscape fez dois anúncios: o primeiro, que Netscape Communicator seria gratuito; segundo, que o código-fonte seria livre.[2] Um dia depois, Jamie Zawinski da Netscape registrou o domínio mozilla.org.[3] O projeto foi batizado como Mozilla em referência ao codinome original do navegador Netscape Navigator que é uma palavra-valise de "Mosaic e Godzilla"[4] e usado para coordenar o desenvolvimento da Mozilla Application Suite, a versão de código aberto do software de internet da Netscape, o Netscape Communicator.[5][6] Jamie Zawinski disse que ele veio com o nome "Mozilla" em uma reunião de pessoal da Netscape.[7][8] Um pequeno grupo de empregados da Netscape foram responsáveis pela coordenação da nova comunidade.
O logo anterior da Mozilla, como desenhado por Shepard Fairey em 1998.

Originalmente, a Mozilla pretendia ser uma provedora de tecnologia para empresas, tal como a Netscape, que comercializaria seu código aberto.[9] Quando a AOL (proprietária da Netscape) reduziu muito o seu envolvimento com a Mozilla em julho de 2003, e a Mozilla Foundation foi designada como o administrador legal do projeto.[10] Logo depois, a Mozilla depreciou a suíte Mozilla Suite em favor de criar aplicações independentes para cada função, principalmente o navegador web Firefox e o cliente de e-mail Thunderbird, e moveu-se para fornecê-los diretamente ao público.[11]

Recentemente, as atividades da Mozilla expandiram para incluir o Firefox em plataformas móveis (primariamente Android),[12] um SO móvel chamado Firefox OS,[13] um sistema de identificação baseada na web chamada Mozilla Persona e uma loja de aplicativos HTML5.[14]

Em um relatório divulgado em novembro de 2012, a Mozilla informou que sua receita total para 2011 foi de 163 milhões de dólares, o que representa um aumento de 33% em relação aos 123 milhões de dólares em 2010. A Mozilla observou que cerca de 85% de sua receita vem do contrato com o Google.[15]

No final de 2013, a Mozilla anunciou um acordo com a Cisco Systems através do qual o Firefox iria baixar e usar uma compilação binária fornecida pela Cisco de um codec de código aberto[16] para reproduzir o formato de vídeo proprietário H.264.[17][18] Como parte do negócio, a Cisco pagaria quaisquer taxas de licenciamento de patentes associadas aos binários que distribui. O CTO da Mozilla, Brendan Eich, reconheceu que esta "não é uma solução completa" e não é "perfeita".[19] Um funcionário na equipe de formatos de vídeo da Mozilla, escrevendo de forma não oficial, justificado[20] pela necessidade de manter sua grande base de usuários, o que seria necessário em futuras batalhas para formatos de vídeo verdadeiramente livres.

Em dezembro de 2013, a Mozilla anunciou financiamento para o desenvolvimento de jogos não-livres[21] através do Game Creator Challenge. No entanto, mesmo aqueles jogos que podem ser liberados sob um software não-livre ou com licença de código aberto devem ser feitos com tecnologias abertas da web e JavaScript de acordo com os critérios de trabalho descritos no anúncio.

Em janeiro de 2017, a empresa aposentou o seu logotipo de dinossauro em favor de um novo logo que inclui a sequência de caracteres "://" de uma URL, renovando-o como "moz://a".[22]
Valores

De acordo com o manifesto da Mozilla,[23] que esboça metas, princípios e um compromisso, "O projeto Mozilla usa uma abordagem baseada na comunidade para criar software de código aberto de classe mundial e desenvolver novos tipos de atividades colaborativas". As menções ao manifesto da Mozilla's manifesto Apenas suas crenças no que diz respeito à internet e à privacidade da internet, e não tem nenhuma menção de quaisquer pontos de vista políticos ou sociais.
Compromisso

De acordo com a Mozilla Foundation:[24]

    A Fundação Mozilla compromete-se a manter e empregar o Manifesto Mozilla em suas atividades. Especificamente, comprometemo-nos a:

        Construir e permitir o desenvolvimento de tecnologias abertas e comunidades que apoiam os princípios do Manifesto;
        Construir e distribuir aos consumidores bons produtos que apoiam os princípios do Manifesto;
        Utilizar os bens gerados pelo Mozilla (propriedade intelectual como por exemplo direitos autorais e marcas; infra-estrutura, recursos financeiros e reputação) para manter a Internet como uma plataforma livre;
        Promover modelos que criem valor econômico para o benefício público; e
        Promover os princípios do Manifesto Mozilla em nosso discurso público e com a indústria da Internet.

Software
Firefox
Ver artigo principal: Mozilla Firefox
Logo do Firefox

O Firefox é um navegador web e o principal produto de software da Mozilla. Está disponível em versões para desktop e dispositivos móveis. O Firefox usa o motor de layout Gecko para renderizar páginas da web, que implementa padrões web atuais e antecipados.[25] No final de 2015, o Firefox tinha aproximadamente 10-11% da quota de mercado dos navegadores web em todo o mundo, tornando-se o quarto navegador mais usado.[26][27][28]

O Firefox se originou-se de uma ramificação do código base da Mozilla por Dave Hyatt, Joe Hewitt e Blake Ross. Eles acreditaram que os requerimentos comerciais do patrocínio da Netscape e recursos característicos voltados aos desenvolvedores comprometeram a utilidade do navegador Mozilla.[29] Para combater o que eles viram como um inchaço de software na Mozilla Suite, eles criaram um navegador web separado com a intenção de substituir a suíte Mozilla.

O Firefox foi originalmente batizado como Phoenix mas o nome foi mudada para evitar conflitos de marca registrada com a Phoenix Technologies. O substituto inicialmente anunciado, Firebird, provocou objeções da comunidade do projeto Firebird.[30][31] O nome atual, Firefox, foi escolhido em 9 de fevereiro de 2004.[32]
Firefox Mobile
Ver artigo principal: Firefox para Android

O Firefox Mobile (codinomeado Fennec) é a versão do navegador web Mozilla Firefox para dispositivos como smartphones e tablets.

O Firefox Mobile usa o mesmo motor de layout Gecko tal como o Mozilla Firefox. Por exemplo, a versão 1.0 usou o mesmo motor que o Firefox 3.6, e o lançamento seguinte, 4.0, compartilhou código com o Firefox 4.0. Seus recursos incluem suporte para HTML5, Firefox Sync, suporte para extensões e navegação por abas.[33]

O Firefox Mobile está atualmente disponível para dispositivos Android 4.0.3 ou superior com um processador ARMv7 ou x86.[34][35] Tristan Nitot, presidente da Mozilla Europe, disse que uma versão para iPhone ou BlackBerry será dificilmente lançada, citando as política de aprovações do iTunes Store da Apple (que proíbem as aplicações que competem com a própria Apple, e proíbem os motores que executam o código baixado) e o sistema operacional limitado da BlackBerry como os motivos.[36]
Firefox OS
Ver artigo principal: Firefox OS

O Firefox OS (nome de projeto: Boot to Gecko, também conhecido como B2G) é um projeto sistema operacional de código aberto descontinuado desenvolvido pela Mozilla que vista suportar aplicativos HTML5 escritos usando tecnologias "abertas da web" ao invés de APIs nativas específicas de plataforma. O conceito por trás do Firefox OS é que todos os softwares acessíveis ao usuário serão aplicativos HTML5, que usam APIs abertas da web para acessar o hardware do dispositivo diretamente através de JavaScript.[37]

Alguns dispositivos usando este SO incluem[38] o Alcatel One Touch Fire, o ZTE Open e o LG Fireweb.
Thunderbird
Ver artigo principal: Mozilla Thunderbird

O Thunderbird é um cliente de e-mail e grupos de notícias livre, de código aberto, e multiplataforma desenvolvido pela comunidade da Mozilla.

Em 16 de julho de 2012, Mitchell Baker anunciou que a liderança da Mozilla tirou a conclusão que a estabilidade permanente era a coisa mais importante para Thunderbird e que a inovação do Thunderbird não era mais uma prioridade para a Mozilla. Na mesma atualização, Baker também sugeriu que a Mozilla tem um caminho para a comunidade inovar em torno do Thunderbird caso seja de escolha da comunidade.[39]
SeaMonkey
Ver artigo principal: SeaMonkey
SeaMonkey logo

Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
Firefox - Mozilla
Enciclopedia Livre
Isso não faz sentido, mas obrigada!
O SeaMonkey (sucessora da Mozilla Application Suite) é uma suíte de internet multiplataforma livre e de código aberto nos quais os componentes do software incluem um navegador web, uma cliente para enviar e receber mensagens de e-mail e grupos de notícias Usenet, um editor de HTML (Mozilla Composer) e o cliente de IRC ChatZilla.

Em de 10 de março de 2005, a Mozilla Foundation anunciou que não iria mais lançar mais nenhuma versão oficial da Mozilla Application Suite acima de 1.7.x, desde que a fundação se foca nas aplicações separadas Firefox e Thunderbird.[40] O SeaMonkey é mantido pelo SeaMonkey Council, que registrou a marca SeaMonkey com ajuda da Mozilla Foundation.[41] A Mozilla Foundation fornece hospedagem do projeto para os desenvolvedores do SeaMonkey. 

Enciclopedia Livre
