NOVOS PROGRAMAS COM DPKG
-------------------------

    posso querer instalar programas que não estão na central de programas do meu ubuntu
    sempre será com a extensão .deb

        então:
        firefox > vsftpd .deb > download do arquivo vsftpd_3.0.3-12_amd64.deb 

        no terminal >
        sudo dpkg -i vsftpd_3.0.3-12_amd64.deb => (-i de install) para instalar um programa baixado na maquina.
         
            caso testemos, iremos ver que o arquivo foi instalado e podemos usar o servidor já (no caso)

        Assim como:
        sudo dpkg -r vsftpd => (-r de remove) para desinstalar o programa

            podendo ser também:
            sudo apt-get remove vsftpd

        (DICA: Caso aconteçam erros devido as dependências do pacote vsftpd não estarem instaladas, você pode executar o comando $ sudo apt-get -f install para tentar resolver esse problema.)
