COMMANDS LINUX ON TERMINAL

    zip linux.zip linux => para zipar um arquivo (zip nome_do_arquivo_zipado que_diretorio_vou_zipar)
        unzip -l linux.zip => verifica o que foi nome_do_arquivo_zipado (DEU ERRO)
        zip -r linux.zip linux => para zipar de verdade é necessário dar o -r para ser recursivo

    unzip linux.zip => descompacta arquivos

    unzip -q linux.zip => -q de quiet(quieto), descompacta arquivos sem mostrar nenhuma informação ao descompactar

    zip -rq linux.zip linux/ => junta dois comandos e faz -rq de recursive quiet

    tar -cz linux > linux.tar.gz => tar: arquivo de compactação (por padrão já é recursivo) / -cz: create and zip / tar -cz diretorio_que_eu_quero_zipar > diretorio_que_eu_quero_zipar.tar.gz

    tar -xz < linux.tar.gz => -xz: xtract of zip / < (da mesma maneira que nós temos o simbolo de > para cuspir um diretorrio ou arquivo, temos o simbolo de <, onde o comando da esquerda está pedindo para ler o comando da esquerda)

    tar -czf linux.tar.gz linux => -czf: create, zip file, ou seja acrescentando o f podemos nomear o arquivo que queremos zipar, e não precisamos colocar o <.

    tar -xzf linux.tar.gz linux => assim funciona também para descompactar

    tar -vxzf linux.tar.gz => por padrão, o tar é mais simples, não apresenta tanta informação, != do zip. Então caso você queira ver as informações é necessário você solicite-a, para isso acrescentamo o v (-vxzf) de verbose (tanto para compactar como para descompactar)

    tar -cjf linux.tar.bz2 linux => há também o compactador bzip2, este que por sua vez é mais eficiente ao que diz respeito ao tamanho final do arquivo zipado que fica menor. para utilizá-lo trocamos de -czf para -cjf e nomeamos o arquivo da seguinte forma nome_do-arquivo.tar.bz2
                    