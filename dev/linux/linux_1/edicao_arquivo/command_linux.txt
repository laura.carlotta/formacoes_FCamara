COMMANDS LINUX ON TERMINAL

    vi mozilla.txt => Estamos usando o vi para a edição do nosos arquivo pelo terminal
                    - botões de seta são usados para navegar pelo arquivo
                    - Insert => serve para incluir texto
                    - Esc => volta para o modo navegação
                    - i => insere no caracter vigente
                    - a => insere no primiero caracter a direita
                    - A => insere ao final da linha vigente
                    - x => deleta o caracter vigente
                    - 11 letra x => estou solicitando que o sistema execute o x 11vezes para que eu não tenha que ficar apagando letra por letra
                    - dd => exclui a linha toda
                    - 5dd => remove 5 linhas
                    - G => vai para a última linha
                    - 1G => 1ª linha
                    - 3G => 3ª linha
                    - $ => vai pro final da linha vigente
                    - 0 => vai para o inicio da linha vigente
                    - yy => copia a linha vigente
                    - 5yy => copia 5 linhas 
                    - p => cola a(s) linha(s) copiada(s)
                    - 10p => cola 10x o mesmo conteúdo
                    - /valor_a_ser_pesquisado => pesquisa algun termo. Com a letra n você navega pelos próximos termos que foram encontrados. com a N você volta para o termo anterior
                    - para salvar => :w (write)
                    - para sair => :q (quiet)
                    - :wq => para salvar e sair
                    - q! => sair sem salvar
                    