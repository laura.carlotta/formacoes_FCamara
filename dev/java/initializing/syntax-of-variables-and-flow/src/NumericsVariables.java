
public class NumericsVariables {
	
	public static void main(String[] args) {
		
		// int = 32bits e sinal +- (De -2^32 a 2^31 - 1, porque tem o 0 no meio. Equivale a mais ou menos 2.000.000.000)
		// para guardar um n�mero maior, precisamos de um numero com 64 bits.
		
		float floatPoint = 3.14f; // or float floatPoint = (float) 1.3; � como se ele n�o coubesse no float
		long bigNumber = 213254656446521321l; // 2^63 - 1
		short smallNumber = 32767; // 16 bits - 1 (2^16 65.536) 
		byte byteNumber = 127; // 2^8 - ( 256 de -128 a 128 - 1  porque tem o 0 no meio) => se for maior ele ir� truncar.
		
	}

}
