
public class TestIR {

    public static void main(String[] args) {

        double salary = 5000.00;

        System.out.println("N�o deixe de pagar os seus impostos!!");
        
        if (salary < 1900.00) {
        	System.out.println("Pelo valor do seu sal�rio, voc� n�o precisa "
        			+ "pagar impostos!");
        	
        } else {
        	if (salary >= 1900.00 && salary <= 2800.00) {
        		System.out.println("O IR a ser pago � de 7.5%, sendo o valor de "
        				+ "R$ 142,00 reais!");
        		
        	} else {
        		if (salary >= 2800.01 && salary <= 3751.00) {
        			System.out.println("O IR a ser pago � 15%, sendo o valor de "
        					+ "R$ 350,00 reais!");
        			
        		} else {
        			if (salary >= 3751.01 && salary <= 4664.00) {
        				System.out.println("O IR a ser pago � de 22.5%, sendo o "
        						+ "valor de R$ 636,00");
        			} else {
        				System.out.println("O IR a ser pago para sal�rios acima "
        						+ "de R$ 4664,01 � de 30%");
        				double salaryWithIR = (double) (salary * 30 / 100);
        				System.out.print("Por seu sal�rio ser de R$ " + salary
        						+ ", o valor a ser pago � de R$ ");
        				System.out.printf("%.2f", salaryWithIR);
        			}
        		}
        	}
        } 	
    }
}
