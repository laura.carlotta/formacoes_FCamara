
public class DoubleAndFloat {
	
	public static void main(String[] args) {
		
		double division = 3.14 / 2;
		System.out.printf("%.2f%n", division);
		
		int otherDisivion = 5 / 2;
		System.out.println(otherDisivion);
		
		double oneMoreDivision = 5 / 2;
		System.out.println(oneMoreDivision);
		
		double lastDivision = 5.0 / 2;
		System.out.println(lastDivision);
		
		float lastDivisionPlus = (float) 5 / 2;
		System.out.println(lastDivisionPlus);
	}
}
