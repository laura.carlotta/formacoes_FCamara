
public class CharacterAndString {

	public static void main(String[] args) {
		
		char letter = 'a';// precisa ser com as pas simples, uma �nica letra. Unicode 16 bits , bate com a tabela asqui
		System.out.println(letter);
		
		char value = 68; // referente a tabela unicode, ou seja o tipo char � do tipo n�merico.
		System.out.println(value); // Retorna letra D. Equivalente aquele short.
		
		char newValue = (char) (value + 1); 
		System.out.println(newValue);
		
		String sentence = "A Laura agora � uma desenvolvedora!";
		System.out.println(sentence);
	}
}
