
public class CondicionalTest {
	
	public static void main(String[] args) {
		
		int age = 28;
		
		if (age >= 18) {
			
			System.out.println("Voc� � maior de idade!");
			
		} else {
			
			if (age >= 16 && age < 18) {
				
				System.out.println("Voc� pode entrar, desde que esteja "
						+ "acompanhado dos seus pais!");
				
			} else {
				
				System.out.println("Desculpe, voc� n�o pode proceguir!");
				System.out.println("Voc� � muito jovem!");
			}
			
		}
		
		
	}

}
