
public class ConditionalTestBoolean {
	
	public static void main(String[] args) {
		
		int age = 13;
		int quantityOfPeople = 3;
		
		if (age >= 18) {
			System.out.println("Voc� pode entrar na Fcamara!");
			
		} else {
			
			if (age >= 14 && quantityOfPeople >= 2) {
				System.out.println("Voc� pode entrar, desde que um respons�vel "
						+ "legal te autorize");
				
			} else {
				System.out.println("Sinto muito! Voc� � jovem ainda para entrar "
						+ "na Fcamara!");
			}
		}
		
	}

}
