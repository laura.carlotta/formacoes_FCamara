
public class ScopeTest {

	public static void main(String[] args) {
		
		int age = 27;
		int quantityOfpeople = 3;
		boolean someone;
		
		if (quantityOfpeople >= 2) {
			someone = true;
			
		} else {
			someone = false;
			
		}
		
		if (age >= 18) {
			System.out.println("Voc� pode entrar na Fcamara!");
			
		} else {
			
			System.out.println("o Valor de acompanhado = " + someone);
			
			if (age >= 14 && someone) {
			// if (age >= 14 && someone == true)
				System.out.println("Voc� pode entrar, desde que um respons�vel "
						+ "legal te autorize");
				
			} else {
				System.out.println("Sinto muito! Voc� � jovem ainda para entrar "
						+ "na Fcamara!");
			}
		}
	}
}
