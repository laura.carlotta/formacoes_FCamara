
public class VariableTester {
    
	public static void main(String[] args) {
		
		System.out.println("Hello world again - New test");
		
		int userAge;
		userAge = 27;
		
		int atualYear;
		atualYear = 2021;
		
		int birthYear;
		birthYear = 1994;
		
		System.out.println("A idade da Laura � " + userAge + " anos!");
		
		userAge = 20 + 7;
		
		System.out.println("A minha idade, como mostrou a solu��o acima � " 
				+ userAge + " anos!");
		
		System.out.println("Seria a mesma coisa que: Pegar o ano que eu nasci,");
		System.out.println("que � o ano de " + birthYear + " e subtrairmos para o ano em que estamos");
		System.out.println("Que � " + atualYear + ".");
		System.out.println("Que seria: " + (atualYear - birthYear) + " anos!");
		
		System.out.println("Podendo ainda fazer as seguintes opera��es:");
		System.out.println("Multiplicando 9 * 3, que d� " + 9*3 + " anos!");
		System.out.println("Dividindo a idade do meu pai por dois - 1, que d� " + (56 / 2 - 1) + " anos!");
	}
}
