
public class ChainedTestOneMore {

	public static void main(String[] args) {
		
		for (int multi = 1; multi <= 10; multi++) {
			for(int cont = 0; cont <= 10; cont++) {
				
				System.out.println(multi + " x " + cont + " = " + (multi * cont));
			}
			
			System.out.println();
		}
	}
}
