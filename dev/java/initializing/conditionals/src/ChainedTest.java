
public class ChainedTest {
	
	public static void main(String[] args) {
		
		
		int tabu = 8;
		int result = 0;
		
		for (int cont = 1; cont <= 10; cont++) {
			result = tabu * cont;
			System.out.println(tabu + " x " + cont + " = " + result);
		}
	}
}
