
public class SummationTestWithWhile {

	public static void main(String[] args) {
		
		int cont = 0;
		int value = 0;
		
		while (cont <= 10) {
			value += cont;
			
			System.out.println(value);
			cont++;
		}
	}
}
