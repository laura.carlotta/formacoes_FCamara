
public class Fatorial {

    public static void main(String[] args) {
        
        int result = 0;

        for (int n = 1; n <= 10; n++) {
            for (int fatorial = 1; fatorial < n; fatorial++) {
                
                result += n * (fatorial - n);
                System.out.print(result); 
            }
            System.out.println();
            result = 0;
        }
    }
}
