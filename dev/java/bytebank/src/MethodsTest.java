
public class MethodsTest {
	
	public static void main(String[] args) {
		
		// Account account = new Account();
		Account lauraCarlotaCustodioAccount = new Account();
		lauraCarlotaCustodioAccount.balance = 7912.56; // Inicio um saldo
		lauraCarlotaCustodioAccount.deposit(1274.00); // 1 // deposito um valor
		System.out.printf("%.2f%n", lauraCarlotaCustodioAccount.balance);
		
		boolean getWithdrawMoney = lauraCarlotaCustodioAccount.withdraw(935.80); // saco dinheiro
		/* Retorno do metodo fica a seu crit�rio se voc� vai usar quando ela for
		devolvida pra voc� */
		System.out.printf("%.2f%n", lauraCarlotaCustodioAccount.balance);
		System.out.println(getWithdrawMoney);
		
		Account wellingtonMarinhoSoares = new Account(); // 2 // nova conta
		wellingtonMarinhoSoares.deposit(10000.00);
		System.out.printf("%.2f%n", wellingtonMarinhoSoares.balance);
		
		boolean sucessTransfer = 
				wellingtonMarinhoSoares.transfer(32000.00, lauraCarlotaCustodioAccount);
		
		if(sucessTransfer) { // transfiro do cliente 1 para o cliente 2
			System.out.println("Transferencia foi feita com sucesso!");
			
		} else {
			System.out.println("Saldo insuficiente!");
		}
		
		System.out.print("O saldo da conta do Well � R$ ");
		System.out.printf("%.2f%n", wellingtonMarinhoSoares.balance);
		
		System.out.print("O saldo da conta da Laura � R$ ");
		System.out.printf("%.2f%n", lauraCarlotaCustodioAccount.balance);
		
		Client lauraCarlotaCustodioHolder = new Client();
		lauraCarlotaCustodioHolder.name = "Laura Carlota Cust�dio";
		lauraCarlotaCustodioAccount.holder = lauraCarlotaCustodioHolder;
		System.out.println(lauraCarlotaCustodioAccount.holder.name);
		
		/*  
			1 - deposit ? Metodo de uma conta, preciso referenciar uma conta para 
		poder cham�-lo. Podemos dar o nome para a variavel com o mesmo nome da 
		classe, deixando a difere�a com a primeira letra minuscula (a da classe 
		� em maiuscula) como vamos us�-la um pouquinho s� aqui e jog�-la fora 
		depois, n�o � necess�rio dar outro nome. (Ela � tempor�rea) Contudo, se 
		a vari�vel guardar dados ou infos que iremos usar mais tarde, a� j� � 
		necess�rio nome�-la. 
		
			2 - Quando invoco o metodo ele j� tem uma referencia de onde vem e 
			passamos o argumento de conta destino. 
			(--> para a conta do well, lugar na memoria)
			
			wellingtonMarinhoSoares.transfer(3200.00, referencia_de_uma_conta) =>
				� diferente de passar uma conta para dentro do metodo, estamos 
				passando a referencia dessa conta, que � aquele numerizinho interno.
		
		*/
		
	}
}
