public class Account {
	double balance;
	int agency;
	int account;
	Client holder;
	
	public void deposit(double value){
		this.balance += value;
	}
	
	public boolean withdraw(double value){
		if (this.balance >= value) {
			this.balance -= value;
			return true;
			
		} else {
			return false;
		}
		
	}
	
	public boolean transfer(double value, Account destiny) {
		if (this.balance >= value) {
			this.balance -= value;
			// destiny.balance += value;
			destiny.deposit(value); // podemos reutilizar um m�todo da propria classe.
			return true;
		}
		return false;
		
	}
}