
public class ReferenceTest {
	
	public static void main(String[] args) {
		
		Account firstAccount = new Account();
		firstAccount.balance = 300.00;
		
		System.out.println("o saldo da primeira conta � R$ " + firstAccount.balance);
		
		Account secondAccount = firstAccount;
		System.out.println("o saldo da segunda conta � R$ " + secondAccount.balance);
		
		secondAccount.balance += 100.00;
		System.out.println("o saldo da segunda conta � R$ " + secondAccount.balance);

		System.out.println("o saldo da primeira conta � R$ " + firstAccount.balance);

		if (firstAccount == secondAccount) {
			System.out.println("yepppppppppp");
			
		} else {
			System.out.println("nada!");
		}
		
		System.out.println(firstAccount); // Mostra que � do tipo conta e que 
		// est� referenciada na mem�ria neste lugar aqui: 762efe5d. sendo assim 
		// para segunda conta.
		
		System.out.println(secondAccount);
		
		if (firstAccount.balance == secondAccount.balance) {
			System.out.println("Essas contas tem o mesmo saldo!");
			
		} else {
			System.out.println("ixxxi, isso t� errado hein rs rs");
		}
	}
	
	/* referencia para o mesmo lugar. 
	 * � como seu eu tivesse duas cartas para o 
	mesmo endere�o. Neste caso, a conta n�o est� sendo copiada e sim essas duas 
	var�veis est�o apontando para o mesmo objeto. Ou seja as cartas s�o as 
	referencias. E o endere�o � o objeto. (As referencias � como se fossem o 
	mesmo endere�o na mem�ria, tipo 3bf4. 
	Temos sim, duas vari�veis apontando para o mesmo lugar. */
}
