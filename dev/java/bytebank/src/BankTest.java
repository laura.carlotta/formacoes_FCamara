
public class BankTest {

	public static void main(String[] args) {
		
		Client laura = new Client();
		laura.cpf = "413.560.666-00";
		laura.name = "Laura Carlota Cust�dio";
		laura.profession = "Desenvolvedora";
		
		Account lauraAccount = new Account();
		lauraAccount.deposit(100);
		
		lauraAccount.holder = laura;
		System.out.println(lauraAccount.holder.name);
		System.out.println(lauraAccount.holder);
		System.out.println(laura);
	}
}
