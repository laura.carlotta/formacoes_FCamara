
public class AccountNoClientTest {

	public static void main(String[] args) {
		
		Account lauraAccount = new Account();
		System.out.println(lauraAccount.balance);
		
		lauraAccount.holder = new Client(); // 2 - Com a referencia feita o retorno d� o valor guardado.
		System.out.println(lauraAccount.holder); // 1 - Sem referencia do cliente, o retorno da null.
		lauraAccount.holder.name = "Laura Carlota Cust�dio";
		System.out.println(lauraAccount.holder.name);
	}
}
