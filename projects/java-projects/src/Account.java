class Account {
	String cpf;
	int agency = 0001;
	int account;
	double balance;
	Client firstHolder;
	Client secondHolder;
	
	public void deposit(double value) {
		this.balance += value;
	}
	
	public boolean withdraw(double value) {
		if (this.balance >= value) {
			this.balance -= value;
			return true;
		}
		return false;
	}
	
	public boolean transfer(double value, Account destiny) {
		if (this.balance >= value) {
			this.balance -= value;
			destiny.deposit(value);
			return true;
		}
		return false;
	}
}
